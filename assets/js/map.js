window.onload = function () {
  var accessToken = 'pk.eyJ1IjoicGV0ZXJzZzgzIiwiYSI6ImNpeGlyZ2g0cTAwMGQycW83N2VhcnJmbnMifQ.O7xg7yiKnJS-64tUQIfs9Q'

  L.MakiMarkers.accessToken = accessToken
  var mymap = L.map('mapid').setView([43.57123468115138, 1.4667659997940063], 17)
  L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
    maxZoom: 20,
    accessToken: accessToken
  }).addTo(mymap)

  var rocketIcon
  var markers = []
  var color

  nodes.forEach((node, index) => {
    color = '#006DF0' // blue (default)
    if (node.status === 'quarantine') {
      color = '#D80027' // red
    } else if (node.status === 'inactive') {
      color = '#FFC13B' // orange
    } else if (node.status === 'registered') {
      color = '#828282' // gray
    }

    rocketIcon = L.MakiMarkers.icon({
      icon: 'danger',
      color: color,
      size: 'm'
    })

    markers[index] = L.marker([node.latitude, node.longitude], {icon: rocketIcon}).addTo(mymap)
    markers[index].bindPopup(node.name)
  })

  $('.node-button').click(function () {
    markers[this.id].openPopup()
  })
}
