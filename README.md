# LoraWebApp

Web plateform to manage connected objects on a school campus.

## Installation

Create a TTN account with a single application.

* Rename `config/ttn.js.dist` in `config/ttn.js`
* Fill `config/ttn.js` with your IDs
