module.exports = {
  autoPK: false,
  attributes: {
    name: {
      type: 'string',
      required: true,
      unique: true
    },
    eui: {
      type: 'string',
      hexadecimal: true,
      required: true,
      unique: true,
      primaryKey: true
    },
    unit: {
      type: 'string',
      defaultsTo: ''
    },
    status: {
      enum: ['registered', 'active', 'inactive', 'quarantine'],
      required: true
    },
    owner: {
      model: 'user',
      required: true
    },
    latitude: {
      type: 'float',
      required: true
    },
    longitude: {
      type: 'float',
      required: true
    },
    description: {
      type: 'string',
      required: true
    },
    messages: {
      collection: 'message',
      via: 'node'
    },
    framesUp: {
      type: 'integer',
      defaultsTo: 0
    },
    framesDown: {
      type: 'integer',
      defaultsTo: 0
    },
    restriction: {
      enum: ['public', 'private'],
      defaultsTo: 'public'
    }
  }
}
