module.exports = {
  attributes: {
    pseudo: {
      type: 'string',
      required: true,
      unique: true,
      minLength: 4,
      maxLength: 16
    },
    password: {
      type: 'string',
      required: 'true'
    },
    email: {
      type: 'email',
      required: true,
      unique: true,
      email: true
    },
    accountType: {
      type: 'string',
      required: true,
      enum: ['admin', 'basic'],
      defaultsTo: 'basic'
    },
    firstName: {
      type: 'string',
      required: true,
      minLength: 1,
      maxLength: 32
    },
    lastName: {
      type: 'string',
      required: true,
      minLength: 1,
      maxLength: 32
    },
    nodes: {
      collection: 'node',
      via: 'owner'
    }
  }
}
