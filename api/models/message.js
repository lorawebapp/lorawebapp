module.exports = {
  autoPK: false,
  attributes: {
    counter: {
      type: 'integer',
      required: true
    },
    sendingDate: {
      type: 'date',
      required: true
    },
    node: {
      model: 'node'
    },
    data: {
      type: 'string',
      required: true
    },
    way: {
      enum: ['up', 'down'],
      required: true
    },
    rssi: {
      type: 'float',
      required: true
    },
    snr: {
      type: 'float',
      required: true
    }
  }
}
