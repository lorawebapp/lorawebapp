module.exports = {

  applicationManager: function (requestMethod, requestUrl, requestBody, callback=null) {
    var request = require('request')

    // get handler
    request.get({url: 'http://discovery.thethingsnetwork.org:8080/announcements/handler/ttn-handler-eu'}, function (err, httpRes, body) {
      if (err) {
        console.log('err : ', err)
        return
      } else if (!err && httpRes.statusCode !== 200) {
        console.log('triste :', httpRes.statusCode)
        return
      }
      var handlerUrl = JSON.parse(body).api_address

      // get token
      var base64 = require('base-64') // On devrait éviter de récupérer le token à chaque fois. Faudrait le storer dans la bse
      var data = {
        username: sails.config.ttn.appId,
        password: sails.config.ttn.accessKey,
        grant_type: 'password'
      }
      request.post({
        url: 'https://account.thethingsnetwork.org/api/v2/applications/token',
        form: data,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + base64.encode(sails.config.ttn.client_name + ':' + sails.config.ttn.client_secret)
        }
      }, function (err, httpResponse, body) {
        if (err) {
          console.log('err : ', err)
          return
        } else if (!err && httpRes.statusCode !== 200) {
          console.log('triste :', httpRes.statusCode)
          return
        }
        var token = JSON.parse(body).access_token

        request({
          method: requestMethod.toLowerCase(),
          url: handlerUrl + requestUrl,
          headers: {
            'Authorization': 'Bearer ' + token
          },
          json: true,
          body: requestBody
        }, callback)
      })
    })
  }
}
