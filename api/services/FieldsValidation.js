module.exports = {

  pseudoIsValid: function (pseudo) {
    return typeof (pseudo) === 'string' && pseudo.length >= 4 && pseudo.length <= 16
  },

  passwordsAreValid: function (pass1, pass2) {
    return typeof (pass1) === 'string' && typeof (pass2) === 'string' && pass1.length >= 6 && pass1.length <= 32 && pass1 === pass2
  },

  firstNameIsValid: function (firstName) {
    return typeof (firstName) === 'string' && firstName.length >= 1 && firstName.length <= 32
  },

  lastNameIsValid: function (lastName) {
    return typeof (lastName) === 'string' && lastName.length >= 1 && lastName.length <= 32
  },

  emailIsValid: function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
  }
}
