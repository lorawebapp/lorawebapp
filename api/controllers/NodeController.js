/**
 * NodeController
 *
 * @description :: Server-side logic for managing Nodes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  processAddNode: function (req, res) {
    let error = {}
    const previousUserInput = {
      private: req.param('restriction'),
      objectName: req.param('object_name') || '',
      description: req.param('description') || '',
      unit: req.param('unit') || '',
      eui: req.param('eui') || '',
      latitude: req.param('latitude') || '',
      longitude: req.param('longitude') || ''
    }

    let newNode = {
      "app_id": sails.config.ttn.appId,
      "dev_id": previousUserInput.objectName,
      "lorawan_device": {
        "activation_constraints": "otaa",
        "app_eui": sails.config.ttn.appEui,
        "app_id": sails.config.ttn.appId,
        "dev_eui": previousUserInput.eui,
        "dev_id": previousUserInput.objectName,
        "disable_f_cnt_check": false
      }
    }

    ApiClients.applicationManager('POST', '/applications/'+ sails.config.ttn.appId + '/devices', newNode, (err, httpResponse, body) => {
      if (err) {
        console.log("Erreur :( " + err)
        return res.redirect('/nodes')
      }

      if (httpResponse.statusCode != 200) {
        console.log('Mauvais ajout : ' + JSON.stringify(httpResponse))
        return res.redirect('/nodes')
      }

      Node.create({
        name: req.param('object_name'),
        eui: req.param('eui'),
        appId: req.param('appId'),
        appKey: req.param('appKey'),
        unit: req.param('unit'),
        status: 'registered',
        owner: req.session.user.id,
        latitude: req.param('latitude'),
        longitude: req.param('longitude'),
        description: req.param('description'),
        restriction: req.param('private') === 'on' ? 'private' : 'public'
      }).exec((err, created) => {
        if (err) {
          if (err.code === 'E_VALIDATION') {
            console.log('err VALIDATION:', err)
            return
          } else {
            return res.negotiate(err)
          }
        }

        return res.redirect('/nodes')
      })
    })
  }
}
