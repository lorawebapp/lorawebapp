var sha1 = require('sha1')
const salt = 'burl3squ3'

module.exports = {

  processRegister: function (req, res) {
    let error = {}
    const previousUserInput = {
      pseudo: req.param('pseudo') || '',
      email: req.param('email') || '',
      firstName: req.param('first_name') || '',
      lastName: req.param('last_name') || ''
    }

    if (!FieldsValidation.firstNameIsValid(req.param('first_name'))) {
      error.message = 'Le prénom est invalide.'
      return res.view('public/register/register', {layout: 'layout', previousUserInput: previousUserInput, error: error})
    }

    if (!FieldsValidation.lastNameIsValid(req.param('last_name'))) {
      error.message = 'Le nom de famille est invalide.'
      return res.view('public/register/register', {layout: 'layout', previousUserInput: previousUserInput, error: error})
    }

    if (!FieldsValidation.pseudoIsValid(req.param('pseudo'))) {
      error.message = 'Le pseudo est invalide.'
      return res.view('public/register/register', {layout: 'layout', previousUserInput: previousUserInput, error: error})
    }

    if (!FieldsValidation.emailIsValid(req.param('email'))) {
      error.message = 'L\'email est invalide.'
      return res.view('public/register/register', {layout: 'layout', previousUserInput: previousUserInput, error: error})
    }

    if (!FieldsValidation.passwordsAreValid(req.param('password'), req.param('password2'))) {
      error.message = 'Les mots de passe sont différents ou invalides.'
      return res.view('public/register/register', {layout: 'layout', previousUserInput: previousUserInput, error: error})
    }

    User.create({
      pseudo: req.param('pseudo').toLowerCase(),
      email: req.param('email').toLowerCase(),
      firstName: req.param('first_name'),
      lastName: req.param('last_name'),
      accountType: 'basic',
      password: sha1(req.param('password') + salt)
    }).exec((err, created) => {
      if (err) {
        if (err.code === 'E_VALIDATION') {
          const invalidAttributes = Object.getOwnPropertyNames(err.invalidAttributes)
          switch (invalidAttributes[0]) {
            case 'pseudo':
              error.message = 'Ce pseudo existe déjà.'
              return res.view('public/register/register', {layout: 'layout', previousUserInput: previousUserInput, error: error})
            case 'email':
              error.message = 'Cet email est déjà utilisé.'
              return res.view('public/register/register', {layout: 'layout', previousUserInput: previousUserInput, error: error})
          }
        } else {
          return res.negotiate(err)
        }
      }

      return res.view('public/register/confirmation', {layout: 'layout', email: req.param('email')})
    })
  },

  processLogin: function (req, res) {
    const email = req.param('email')
    const password = sha1(req.param('password') + salt)
    let error = {}
    let previousUserInput = {email: email}

    User.findOne({email: email, password: password}).exec((err, found) => {
      if (err) {
        return res.negotiate(err)
      }

      if (!found) {
        error.message = 'Email ou mot de passe invalide.'
        return res.view('public/login', {layout: 'layout', error: error, previousUserInput: previousUserInput})
      }

      req.session.authenticated = true
      req.session.user = found

      return res.redirect('/')
    })
  },

  processLogout: function (req, res) {
    req.session.authenticated = false
    req.session.user = null

    return res.redirect('/')
  },

  nodes: function (req, res) {
    Node.find({owner: req.session.user.id}).populate('owner').sort('createdAt ASC').exec((err, found) => {
      if (err) {
        return res.negotiate(err)
      }

      return res.view('user/nodes', {layout: 'layout', nodes: found})
    })
  },

  explore: function (req, res) {
    if (req.session.authenticated) {
      Node.find({or: [
        {restriction: 'public'},
        {owner: req.session.user.id}
      ]}).populate('owner').sort('name DESC').exec((err, found) => {
        if (err) {
          return res.negotiate(err)
        }

        var publicNodes = found.reduce((total, current) => {
          if (current.restriction === 'public') {
            total.push(current)
          }
          return total
        }, [])

        var userNodes = found.reduce((total, current) => {
          if (current.owner.id === req.session.user.id) {
            total.push(current)
          }
          return total
        }, [])

        return res.view('public/explore', {layout: 'layout', allNodes: found, publicNodes: publicNodes, userNodes: userNodes})
      })
    } else {
      Node.find({restriction: 'public'}).populate('owner').sort('name ASC').exec((err, found) => {
        if (err) {
          return res.negotiate(err)
        }
        return res.view('public/explore', {layout: 'layout', publicNodes: found})
      })
    }
  }
}
