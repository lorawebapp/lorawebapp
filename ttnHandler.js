var ttn = require('ttn')

module.exports = {
  listen: function () {
    console.log('Starting to listen')
    var client = new ttn.Client(sails.config.ttn.region, sails.config.ttn.appId, sails.config.ttn.accessKey);

    client.on('connect', function (connack) {
      console.log('In connect')
      console.log('[DEBUG]', 'Connect:', connack)
    })

    client.on('error', function (err) {
      console.log('In error')
      console.error('[ERROR]', err.message)
    })

    client.on('message', function (deviceId, data) {
      console.info('[INFO] ', 'Message:', deviceId, JSON.stringify(data, null, 2))

      Node.findOne({name: deviceId}, function (err, found){
        message.create({
          counter: data.counter,
          node: found.id,
          sendingDate: data.metadata.time,
          rssi: data.metadata.gateways[0].rssi,
          snr: data.metadata.gateways[0].snr,
          data: data.payload_raw.toString(),
          way: 'up'
        }, function (err, created) {
          if (err) {
            console.log(err)
          }
        })
      })
    })
  }
}
